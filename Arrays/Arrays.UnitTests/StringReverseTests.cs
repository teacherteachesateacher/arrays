﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Arrays.UnitTests
{
    public class StringReverseTests
    {
        [Test]
        public void ReverseString_EvenLength()
        {
            string input = "1234";

            Assert.IsTrue(input.Reverse().SequenceEqual(input.DashaReverseString()));
        }
        [Test]
        public void ReverseString_OddLength()
        {
            string input = "12345";

            Assert.IsTrue(input.Reverse().SequenceEqual(input.DashaReverseString()));
        }
        [Test]
        public void ReverseString_OneSymbol()
        {
            string input = "1";

            Assert.IsTrue(input.Reverse().SequenceEqual(input.DashaReverseString()));
        }
        [Test]
        public void ReverseString_TwoSymbolsLength()
        {
            string input = "12";

            Assert.IsTrue(input.Reverse().SequenceEqual(input.DashaReverseString()));
        }
    }
}
