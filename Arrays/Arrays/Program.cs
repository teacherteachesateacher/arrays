﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input string for reversing:");
            string input = Console.ReadLine();

            Console.WriteLine("Result:");
            Console.WriteLine(new string(input.DashaReverseString()));
        }
    }
}
